<?php

/**
 * @file
 * Here we override the default HTML output of drupal.
 */

/**
 * Implements theme_status_messages().
 *
 * Use version in uw_core_theme. Using hook_theme() to do this does not work
 * properly: The messages are set too late and appear on the next page load.
 */
function uw_adminimal_theme_status_messages($variables) {
  require_once DRUPAL_ROOT . '/' . drupal_get_path('theme', 'uw_core_theme') . '/template.php';
  return uw_core_theme_status_messages($variables);
}

/**
 * Override or insert variables into the html template.
 */
function uw_adminimal_theme_preprocess_html(&$vars) {

  // Get folder path.
  $path = drupal_get_path('theme', 'uw_adminimal_theme');

  // Add default styles in a way that will put them *after* the Adminimal
  // styles.
  drupal_add_css($path . '/css/admin.css', array('group' => CSS_THEME, 'media' => 'all', 'weight' => 2));

  drupal_add_js($path . '/js/uw_adminimal_theme.js', 'file');

  // Add defined suffix to all page titles. Default to University.
  if (isset($vars['head_title'])) {
    $vars['head_title'] .= check_plain(variable_get('uw_theme_title_append', ' | University of Waterloo'));
  }
}
