/**
 * @file
 */

(function ($) {
  $(document).ready(function () {

    // Sets a fixed position for form actions if they appear below the fold.
    var edit_actions = $('#edit-actions:not(.container-inline)').offset();
    if (edit_actions && edit_actions.top > $(window).height() - 10) {
      $('body').addClass('fixed-actions');
    }

  });
}(jQuery));
